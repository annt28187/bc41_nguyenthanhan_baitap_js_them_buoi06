// Bài 1.

function checkPrimeNumber(number) {
  if (number < 2) return false;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

function printPrime() {
  var inputNumberEl = document.getElementById("inputNumber").value * 1;
  var result = "";
  if (inputNumberEl == 0 || inputNumberEl == 1) {
    alert("Nhập số n lớn hơn bằng 2");
  } else {
    for (var i = 2; i <= inputNumberEl; i++) {
      if (checkPrimeNumber(i)) {
        result += " " + i;
      }
    }
    document.getElementById(
      "txtResult"
    ).innerHTML = ` Các số nguyên tố nhỏ hơn hoặc bằng ${inputNumberEl}: ${result} `;
  }
}
